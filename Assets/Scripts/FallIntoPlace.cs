﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FallIntoPlace : MonoBehaviour
{
    void Start()
    {
        float targetY = transform.position.y;
        transform.Translate(new Vector3(0, 10, 0));
        transform.DOMoveY(targetY, Random.Range(0.5f, 1.5f)).SetEase(Ease.OutBounce);
    }
}
