﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpManager : MonoBehaviour
{
    public GameObject[] powerUpPrefabs;
    public Settings settings;

    public enum PowerUpType
    {
        Shield,
        HealthUp
    }

    private void Update()
    {
        if (Random.value < settings.powerUpSpawnChancePerSecond * Time.deltaTime)
        {
            SpawnPowerUp();
        }
    }

    public void SpawnPowerUp()
    {
        int randomXValue = Random.Range(-7, 7);
        Vector3 randomPosition = new Vector3(randomXValue, 7, 0);
        int rndm = Random.Range(0, powerUpPrefabs.Length);
        Instantiate(powerUpPrefabs[rndm], randomPosition, Quaternion.identity);
    }
}
