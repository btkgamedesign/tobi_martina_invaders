﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StateManager : MonoBehaviour
{
    public static StateManager Instance;
    public Settings settings;
    public GameObject playerPrefab;
    public Text youLost;
    public Text youWin;
    public Text loadingNextLevel;
    public Text livesValueUI;

    public int lives;

    void Start()
    {
        Instance = this;
        lives = settings.lives;
        SpawnPlayer();
        StartCoroutine(Effects.Blink(Player.Instance.gameObject, settings.blinkAmount, settings.invisibilityDuration, Effects.CallType.Player));
        youLost.enabled = false;
        youWin.enabled = false;
        loadingNextLevel.enabled = false;
    }

    public void RestartGame()
    {
        SceneManager.LoadScene("MainScene");
    }

    public void QuitGame()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

    public void CheckAmountOfInvaders()
    {
        if (InvaderCounter.Instance.invaderCount == 0)
        {
            youWin.enabled = true;
            loadingNextLevel.enabled = true;
            StartCoroutine(LoadSceneWithDelay(2, 2f));
        }
    }

    IEnumerator LoadSceneWithDelay(int buildIndex, float delay)
    {
        yield return new WaitForSeconds(delay);
        SceneManager.LoadScene(buildIndex);
    }

    public void LooseLife()
    {
        lives--;
        livesValueUI.text = lives.ToString();
        if (lives > 0)
        {
            //SpawnPlayer();
            StartCoroutine(Effects.Blink(Player.Instance.gameObject, settings.blinkAmount, settings.invisibilityDuration, Effects.CallType.Player));
            //blink 3 times
        }
        else
        {
            Player.Instance.gameObject.SetActive(false);
            GameOver();
            //Debug.LogError("Game over - Player lost all lives");
        }
    }

    void GameOver()
    {
        youLost.enabled = true;
    }

    void SpawnPlayer()
    {
        Instantiate(playerPrefab);
    }
}
