﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvaderCounter : MonoBehaviour
{
    private static InvaderCounter _instance;
    public static InvaderCounter Instance
    {
        get { return _instance; }
    }
    public int invaderCount;

    private void Awake()
    {
        _instance = this;
    }
}
