﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Effects : MonoBehaviour
{

    public enum CallType
    {
        Player,
        Invader
    }

    public static IEnumerator Blink (GameObject gameObject, int blinkAmount, float blinkDuration, CallType callType)
    {
        if (callType == CallType.Player)
            gameObject.GetComponent<Collider>().enabled = false;
        GameObject image = gameObject.transform.GetChild(0).gameObject;
        bool visible = true;
        for (int i = 0; i < blinkAmount * 2; i++)
        {
            if (visible)
            {
                // turn color off
                image.SetActive(false);
                yield return new WaitForSeconds(blinkDuration / 2f);
                visible = !visible;
            }
            else
            {
                // turn color on
                image.SetActive(true);
                yield return new WaitForSeconds(blinkDuration);
                visible = !visible;
            }
        }
        image.SetActive(true);
        if (callType == CallType.Player)
            Player.Instance.gameObject.GetComponent<Collider>().enabled = true;
    }
}
