﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/Settings", order = 1)]
public class Settings : ScriptableObject
{
    public float spacing;
    public float speedFactor;
    public float invaderMoveRange;
    public float invaderSpeedFactor;
    public float invaderShotChancePerSecond;
    public float bulletSpeed;
    public float invisibilityDuration;
    public float powerUpSpawnChancePerSecond;
    public int lives;
    public int blinkAmount;
}