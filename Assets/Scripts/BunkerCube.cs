﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BunkerCube : MonoBehaviour
{
    void OnTriggerEnter(Collider other)

    {
        Bullet b = other.gameObject.GetComponent<Bullet>();
        if (b != null)
        //intersected with a bullet
        {
            gameObject.SetActive(false);
            Destroy(b.gameObject);
        }
        else
        {
            Invader i = other.gameObject.GetComponent<Invader>();
            //intersected with an Invader
            if (i != null)
            {
                gameObject.SetActive(false);
            }

        }
    }
}
