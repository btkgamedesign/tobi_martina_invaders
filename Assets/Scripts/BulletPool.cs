﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletPool : MonoBehaviour
{
    private static BulletPool _instance;
    public static BulletPool Instance { get { return _instance; } }
    public Settings settings;

    public GameObject bulletPrefab;

    public enum CallType
    {
        Player,
        Invader
    }

    private void Awake()
    {
        _instance = this;
    }

    public void CreateAndShootBullet(Transform shootLocation, CallType callType)
    {
        GameObject bullet = Instantiate(bulletPrefab);
        bullet.GetComponent<Bullet>().shotBy = shootLocation.gameObject;

        if (callType == CallType.Player)
        {
            bullet.transform.position = shootLocation.position + Vector3.up * 1.1f;
            bullet.GetComponent<Bullet>().Shoot(Vector3.up);
        }
        else
        {
            bullet.transform.position = shootLocation.position + Vector3.down * 1.1f;
            bullet.GetComponent<Bullet>().Shoot(Vector3.down);
        }
    }
}
