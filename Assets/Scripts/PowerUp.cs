﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.Build.Player;
using UnityEngine;
using UnityEngine.Assertions.Must;

public class PowerUp : MonoBehaviour
{
    public PowerUpManager.PowerUpType powerUpType;
    public float fallSpeed = 2;

    private void Start()
    {
        
    }

    private void Update()
    {
        Vector3 fall = new Vector3(transform.position.x, transform.position.y - (1 / fallSpeed)* Time.deltaTime, 0);
        transform.position = fall;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawSphere(transform.position, 0.5f);
        Gizmos.DrawWireSphere(transform.position, 0.5f);
    }

    private void ShieldPlayer()
    {
        Player.Instance.isShielded = true;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Bullet"))
        {
            Bullet b = other.gameObject.GetComponent<Bullet>();
            if (b.shotBy.gameObject.CompareTag("Player"))
            {
                switch (powerUpType)
                {
                    case PowerUpManager.PowerUpType.Shield:
                        ShieldPlayer();
                        break;
                    case PowerUpManager.PowerUpType.HealthUp:
                        StateManager.Instance.lives++;
                        StateManager.Instance.livesValueUI.text = StateManager.Instance.lives.ToString();
                        break;
                    default:
                        break;
                }
                Destroy(b.gameObject);
                Destroy(this.gameObject);
            }
        }
        if (other.gameObject.CompareTag("Floor"))
        {
            Debug.Log("You've lost a PowerUp maaaan");
        }
    }
}
