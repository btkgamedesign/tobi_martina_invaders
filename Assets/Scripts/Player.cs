﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private static Player _instance;
    public static Player Instance { get { return _instance; } }
    public Settings settings;
    public GameObject shield;
    public bool isShielded;
    public float leftLimit;
    public float rightLimit;
    //public float speedFactor;

    private void Awake()
    {
        _instance = this;
        isShielded = false;
    }

    void Update()
    {
        //move player ship right
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Translate(new Vector3(settings.speedFactor * Time.deltaTime, 0, 0));
        }

        //move player ship left
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Translate(new Vector3(-settings.speedFactor * Time.deltaTime, 0, 0));
        }

        //shoot bullet
        if (Input.GetKeyDown(KeyCode.Space))
        {
            BulletPool.Instance.CreateAndShootBullet(this.transform, BulletPool.CallType.Player);
        }

        //keep player in bounds
        float newX = Mathf.Clamp(transform.position.x, leftLimit, rightLimit);
        if (transform.position.x != newX)
        {
            transform.position = new Vector3(newX, transform.position.y, transform.position.z);
        }

        CheckShieldStatus();
    }

    private void CheckShieldStatus()
    {
        if (isShielded)
        {
            shield.GetComponent<SpriteRenderer>().enabled = true;
        }
        else
        {
            shield.GetComponent<SpriteRenderer>().enabled = false;
        }
    }

    void OnTriggerEnter(Collider other)

    {
        Bullet b = other.gameObject.GetComponent<Bullet>();
        if (b != null)
        //intersected with a bullet
        {
            Destroy(b.gameObject);
            if (isShielded)
            {
                isShielded = !isShielded;
            }
            else
            {
                StateManager.Instance.LooseLife();
            }
        }
        else
        {
            Invader i = other.gameObject.GetComponent<Invader>();
            if (i != null)
            {
                Debug.LogError("Game Over. You died, your spaceship collided an enemy spaceship.");
            }
        }
    }
}
