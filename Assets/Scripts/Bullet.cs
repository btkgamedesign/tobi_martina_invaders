﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public Settings settings;
    Vector3 originalPosition;
    public GameObject shotBy;

    void Start()
    {
        originalPosition = transform.position;
    }

    public void Shoot(Vector3 direction)
    {
        GetComponent<Rigidbody>().velocity = direction * settings.bulletSpeed;
    }

    private void OnCollisionStay(Collision collision)
    {
        Destroy(this.gameObject);
    }
}
