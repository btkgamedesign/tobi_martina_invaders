﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Invader : MonoBehaviour
{
    public Settings settings;
    public ParticleSystem deathEffect;
    public AudioSource audioSource;
    float leftLimit;
    float rightLimit;
    bool goingRight;
    int lives;
    //var invaderRenderer;

    void Start()
    {
        leftLimit = transform.position.x - settings.invaderMoveRange;
        rightLimit = transform.position.x + settings.invaderMoveRange;
        goingRight = true;
        lives = Random.Range(1, 3);
    }

    public void SetSprite(Sprite s)
    {
        GetComponentInChildren<SpriteRenderer>().sprite = s;
    }

    private void Flip()
    {
        goingRight = !goingRight;
        transform.Translate(0, -.5f, 0); //go down
    }

    void Update()
    {
        float moveX = 0;
        if (goingRight) //right movement
        {
            if (transform.position.x > rightLimit)
            {
                Flip();
            }
            else
            {
                moveX = settings.invaderSpeedFactor * Time.deltaTime;
            }
        }
        else //left movement
        {
            if (transform.position.x < leftLimit)
            {
                Flip();
            }
            else
            {
                moveX = -settings.invaderSpeedFactor * Time.deltaTime;
            }
        }

        transform.Translate(moveX, 0, 0);

        //shoot randomly
        if (Random.value < settings.invaderShotChancePerSecond * Time.deltaTime)
        {
            int layerMask = 1 << LayerMask.NameToLayer("invader");
            bool hasInvaderUnderMe = Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), 10, layerMask);
            if (!hasInvaderUnderMe) //only if there is no invader below
            {
                BulletPool.Instance.CreateAndShootBullet(this.transform, BulletPool.CallType.Invader);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Bullet bullet = other.gameObject.GetComponent<Bullet>();
        if (bullet != null) //intersected with a bullet
        {
            Destroy(bullet.gameObject);
            if (lives > 0)
            {
                StartCoroutine(Effects.Blink(gameObject, settings.blinkAmount, settings.invisibilityDuration, Effects.CallType.Invader));
                lives--;
            }
            else
            {
                audioSource.Play();
                gameObject.SetActive(false);
                InvaderCounter.Instance.invaderCount--;
                StateManager.Instance.CheckAmountOfInvaders();
                ParticleSystem particle = Instantiate(deathEffect, transform.position, Quaternion.identity);
                Destroy(particle, particle.main.duration);
            }
        }
    }
}
