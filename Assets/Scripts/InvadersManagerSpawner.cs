﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvadersManagerSpawner : MonoBehaviour
{
    public GameObject invadersManagerPrefab;
    public Transform spawnLocation;
    public Settings settings;
    public float timer = 2f;

    private void Update()
    {
        timer -= Time.deltaTime;
        if (timer <= 0f)
        {
            Debug.Log("spawned another wave");
            Instantiate(invadersManagerPrefab, spawnLocation.position, Quaternion.identity);
            timer = 2f;
        }
    }
}
