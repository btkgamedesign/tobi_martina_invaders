﻿using System.Collections;
using System.Collections.Generic;
using UnityEditorInternal;
using UnityEngine;

public class InvadersManager : MonoBehaviour
{
    private static InvadersManager _instance;
    public static InvadersManager Instance
    {
        get { return _instance; }
    }
    public Settings settings;

    public GameObject invaderPrefab;
    [HideInInspector]
    public List<GameObject> invaders;

    public List<Sprite> invaderGraphics;
    public List<ParticleSystem> invaderDeathEffects;

    public bool destroySelfAfterInvaderSpawn;

    public int startingInvaders;
    public int lines;

    private void Awake()
    {
        _instance = this;
    }

    void Start()
    {
        invaders = new List<GameObject>();
        int invadersPerLine = startingInvaders / lines;

        for (int x = 0; x < invadersPerLine; x++)
        {
            for (int y = 0; y < lines; y++)
            {
                GameObject invader = Instantiate(invaderPrefab, transform);
                invaders.Add(invader);

                invader.GetComponent<Invader>().SetSprite(invaderGraphics[y%3]);
                invader.GetComponent<Invader>().deathEffect = invaderDeathEffects[y%3];

                invader.transform.localPosition = new Vector3(settings.spacing * (x - invadersPerLine / 2f + 0.5f), settings.spacing * -y, 0);
                invader.name = $"<{x},{y}> invader";

                InvaderCounter.Instance.invaderCount++;
            }
        }

        if (destroySelfAfterInvaderSpawn)
        {
            Destroy(this.gameObject, 3f);
        }
    }
}
